# Covela challenge front
> This is the visual part of Covela challenge, that permits to see wether the data was succesfully scrapped or not, and the data that was scrapped. Live version on https://covela-front.firebaseapp.com/#/

![](sample.png)

## Getting Started

You can download this project from git, using git clone

### Prerequisites

You'll need node > 8, npm > 5

### Installing

First, git clone the project, then run

```sh
npm i
```

```sh
npm run serve
```

## Running the tests

In order to run the test, you'll need chrome as they've been developed using cypress

### Testing the page

In those tests, the homepage and the character page are tested.

For the home page :

- The first test is about checking that after a short amount of time, we can see the scraper status and the characters scrapped

- The second test is about checking that clicking on a character will redirect on a character page

- The third test is about checking that using the search bar will filter the characters

For the character page :

- The only test is about checking data for a specific character

### Running locally the tests

```sh
npm run test:e2e
```

## Deployment

The easiest way to deploy this spa, is through firebase.
More info here :
https://firebase.google.com/docs/hosting/deploying

## Built With

* [Vuejs](https://vuejs.org/)
