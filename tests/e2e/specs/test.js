// https://docs.cypress.io/api/introduction/api.html

describe("when visiting /", () => {
  it("displays search + scrapper status + some character cards", () => {
    cy.visit("/");
    cy.wait(3000);
    cy.get("input#search").should("be.visible");
    cy.get("div.status-box .card-content").should("be.visible");
    cy.get("div.status-box .card-content p").contains("success");
    cy.get("a.character-card").should("be.visible");
  });
  describe("when clicking on character card", () => {
    it("should display character page", () => {
      cy.visit("/");
      cy.wait(3000);
      cy.get("a.character-card").first().click();
      cy.url().should("contains", "/characters/");
      cy.get(".picture img").should("be.visible");
      cy.get(".text").should("be.visible");
    });
  });
  describe("when using search box", () => {
    it("should filter characters", () => {
      cy.visit("/");
      cy.wait(3000);
      cy.get("input#search").type("naruto uzum", { force: true });
      cy.get("a.character-card p").contains("Naruto Uzumaki");
    });
  });
});

describe("when visiting character page", () => {
  it("should display character page", () => {
    cy.visit("/#/characters/5b2449f50c96db8241d0464a");
    cy.wait(1000);
    cy.get(".picture img").should("be.visible");
    cy.get(".picture p").contains("Hinata and Hanabi's Mother");
    cy.get(".text").should("be.visible");
  });
});
